#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env


mkdir -p ${MXTEST_HOME}/data/matrix-appservice-irc
mkdir -p ${MXTEST_HOME}/data/matrix-appservice-irc-db

cp ${MXTEST_SDK_ROOT}/bridge/matrix-appservice-irc/config.yaml ${MXTEST_HOME}/data/matrix-appservice-irc/config.yaml

# TODO create a container for this
openssl genpkey -out ${MXTEST_HOME}/data/matrix-appservice-irc/passkey.pem -outform PEM -algorithm RSA -pkeyopt rsa_keygen_bits:${MXTEST_RSA_DEFAULT_KEYSIZE}

docker run --rm \
    --cap-drop=ALL \
    --mount type=bind,src=${MXTEST_HOME}/data/matrix-appservice-irc,dst=/config \
    -e UID=${MXTEST_UID} \
    -e GID=${MXTEST_GID} \
    --user ${MXTEST_UID}:${MXTEST_GID} \
    --entrypoint=/bin/bash \
    docker.io/matrixdotorg/matrix-appservice-irc:latest -c 'node app.js -r -f /config/appservice-registration-irc.yaml -u "http://matrix-appservice-irc:8090" -c /config/config.yaml -l irc_bot'
