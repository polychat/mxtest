#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env

mkdir -p ${MXTEST_HOME}/data/polychat-appservice

cp ${MXTEST_SDK_ROOT}/bridge/polychat-appservice/registration.yaml ${MXTEST_HOME}/data/polychat-appservice/registration.yaml
