#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env


mkdir -p ${MXTEST_HOME}/data/bifrost
mkdir -p ${MXTEST_HOME}/data/bifrost-db

cp ${MXTEST_SDK_ROOT}/bridge/bifrost/config.yaml ${MXTEST_HOME}/data/bifrost/config.yaml

# TODO create a container for this
#openssl genpkey -out ${MXTEST_HOME}/data/bifrost/passkey.pem -outform PEM -algorithm RSA -pkeyopt rsa_keygen_bits:${MXTEST_RSA_DEFAULT_KEYSIZE}

docker run --rm \
    --cap-drop=ALL \
    --mount type=bind,src=${MXTEST_HOME}/data/bifrost,dst=/data \
    -e UID=${MXTEST_UID} \
    -e GID=${MXTEST_GID} \
    --user ${MXTEST_UID}:${MXTEST_GID} \
    --entrypoint=/bin/bash \
    docker.io/matrixdotorg/matrix-bifrost:latest -c 'yarn genreg -f /data/registration.yaml -c /data/config.yaml -u "http://bifrost:9555"'