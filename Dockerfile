FROM docker.io/library/docker:24-cli

RUN adduser -s /bin/sh -g "mxtest" -D mxtest

RUN apk upgrade && \
    apk add git openssl curl yq && \
    rm -rf /var/cache/apk/

COPY . /mxtest/

ENV MXTEST_SDK_ROOT /mxtest
