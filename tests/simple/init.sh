#!/bin/sh

set -eu

# remove old data if present, mxtest init expects an empty working dir
rm -rf ./data
rm -f .env

# create default env file
${MXTEST_SDK_ROOT}/bin/init.sh > .env
