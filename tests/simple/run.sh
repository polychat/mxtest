#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env

docker compose up -d --wait

docker compose exec synapse register_new_matrix_user -c /data/homeserver.yaml -u testadmin -p testadmin -a

docker compose exec synapse register_new_matrix_user -c /data/homeserver.yaml -u testuser -p testuser --no-admin
