#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env


mkdir -p ${MXTEST_HOME}/data/element/nginx-cache
mkdir -p ${MXTEST_HOME}/data/element/run
mkdir -p ${MXTEST_HOME}/data/element/config

cp ${MXTEST_SDK_ROOT}/client/element/config.json ${MXTEST_HOME}/data/element/config/config.json