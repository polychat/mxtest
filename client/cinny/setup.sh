#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env


mkdir -p ${MXTEST_HOME}/data/cinny/nginx-cache
mkdir -p ${MXTEST_HOME}/data/cinny/run
mkdir -p ${MXTEST_HOME}/data/cinny/config

cp ${MXTEST_SDK_ROOT}/client/cinny/config.json ${MXTEST_HOME}/data/cinny/config/config.json