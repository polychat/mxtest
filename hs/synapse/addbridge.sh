#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env

cp $1 ${MXTEST_HOME}/data/synapse/$2

docker run --rm \
    --mount type=bind,src=${MXTEST_HOME}/data/synapse,dst=/data \
    --mount type=bind,src=${MXTEST_SDK_ROOT}/hs/synapse,dst=/scripts \
    -e UID=${MXTEST_UID} \
    -e GID=${MXTEST_GID} \
    --user ${MXTEST_UID}:${MXTEST_GID} \
    --entrypoint /bin/bash \
    docker.io/matrixdotorg/synapse:latest -c "python /scripts/addbridge.py /data/homeserver.yaml /data/$2"
