import sys
import yaml

def confighacker(hsconfig, filename):
    with open(hsconfig, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    try:
        cfg['app_service_config_files'].append(filename)
    except KeyError:
        cfg['app_service_config_files'] = [filename]

    with open(hsconfig, 'w') as f:
        yaml.dump(cfg, f)

if __name__ == "__main__":
    confighacker(sys.argv[1], sys.argv[2])
