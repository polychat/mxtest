#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env

docker run --rm \
    --mount type=bind,src=${MXTEST_HOME}/data/,dst=/data \
    docker.io/library/alpine:latest /bin/sh -c "cd /data; rm -rf *"

rm ${MXTEST_HOME}/.env
